﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class GameInitializeSystem
{
	/**
	 * 
	 */
	[RuntimeInitializeOnLoadMethod]
	public static void Initialize()
	{
		
		Debug.Log("Initialize");
		SceneManager.LoadSceneAsync("GameSystemBaseManager",LoadSceneMode.Additive).completed += operation =>
		{
			Debug.Log("LoadSceneAsync Complete");			
		};
		//SceneManager.MergeScenes(SceneManager.GetActiveScene(),SceneManager.GetSceneByName("GameSystemBaseManager"));
	}
}
