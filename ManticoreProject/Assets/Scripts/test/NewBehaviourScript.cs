﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    /*
    ゲーム終了時にも実行されるので使ってはいけない
    public NewBehaviourScript()
    {
        Debug.Log("ここだとGameSystemManagerが後にくる");
    }
    */
    private void Awake()
    {
        Debug.Log("ここだとGameSystemManagerが後にくる");
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("ここならGameSystemManagerが先にくる");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
