﻿using System;
using System.Runtime.InteropServices;
using Boo.Lang;
using Common;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class SceneManagementScript : SingletonMonoBehaviour<SceneManagementScript>
    {
        // 現在のシーン
        private Scene _currentScene;
        
        // 過去のシーンリスト
        private List<SceneData> SceneList = new List<SceneData>();

        private void Awake()
        {
        }

        // シーンの保存()
        public void SaveScene()
        {
        }
        
        // ひとつ前のシーンへ戻る
        public void BackScene()
        {
        }
        
        // 次のシーンへ遷移
        public void NextScene()
        {
        }
    
        // シーン変更
        public void ChangeScene()
        {
        }
    }

    public class SceneData
    {
        private Scene scene;
        public Scene Scene
        {
            get { return scene; }
            set { scene = value; }
        }

        public SceneData(string sceneName = null)
        {
            Scene = SceneManager.GetActiveScene();

            Scene = new Scene()
            {
            };
        }
        
    }

    public class SceneOptionData
    {
        public string Key;
        public string Value;
    }
}
