﻿using Common;
using UnityEngine;

namespace Manager
{
    public class DataBaseManagementScript : SingletonMonoBehaviour<DataBaseManagementScript>
    {

        public DataBaseManagementScript()
        {
            // ここにはUnityのオブジェクトには干渉できない
        }
    }
}
