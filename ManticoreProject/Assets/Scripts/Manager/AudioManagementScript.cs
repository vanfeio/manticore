﻿using Common;
using UnityEngine;
using UnityEngine.Audio;

namespace Manager
{
    public enum AudioClipType
    {
        BGM,
        SE,
        VOICE
    }

    public class AudioManagementScript : SingletonMonoBehaviour<AudioManagementScript>
    {
        //オーディオ管理Script
        public AudioListener AudioListener;
        public AudioMixer AudioMixer;

        private void Awake()
        {
            if(AudioListener == null) AudioListener = CreateClassObject<AudioListener>("AudioListener");
        }
        
        

        public void SetAudioMixerParameter(AudioClipType act, float volume)
        {
            AudioMixer.SetFloat(act.ToString() + "_VOLUME", volume);
        }

        public float GetAudioMixerParameter(AudioClipType act)
        {
            float result;
            AudioMixer.GetFloat(act.ToString() + "", out result);
            return result;
        }

    }
}
