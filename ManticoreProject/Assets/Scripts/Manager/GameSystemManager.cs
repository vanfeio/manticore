﻿using System.Runtime.CompilerServices;
using Common;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
	public class GameSystemManager : SingletonMonoBehaviour<GameSystemManager>
	{
		public AudioManagementScript AudioManagement;
		public SceneManagementScript SceneManagement;
		public DataBaseManagementScript DataManagement;
		public ResourceManagementScript ResourceManagement;

		private void Awake()
		{
			Debug.Log("GameSystemManager起動");
			DontDestroyOnLoad(this.gameObject);
		}

		//[RuntimeInitializeOnLoadMethod]
		private static void GameInitialize()
		{
			DontDestroyOnLoad(Instance);
		}
		
		
	}
}
